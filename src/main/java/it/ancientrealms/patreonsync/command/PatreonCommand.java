package it.ancientrealms.patreonsync.command;

import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.CommandIssuer;
import co.aikar.commands.InvalidCommandArgument;
import co.aikar.commands.MessageType;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Conditions;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Dependency;
import co.aikar.commands.annotation.Description;
import co.aikar.commands.annotation.HelpCommand;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.annotation.Syntax;
import co.aikar.locales.MessageKey;
import it.ancientrealms.patreonsync.PatreonSync;
import it.ancientrealms.patreonsync.Patron;

@CommandAlias("patreon")
public final class PatreonCommand extends BaseCommand
{
    @Dependency
    private PatreonSync plugin;

    @Default
    @HelpCommand
    public void showHelp(CommandSender sender, CommandHelp help)
    {
        help.showHelp();
    }

    @Subcommand("associa")
    @Syntax("<email>")
    @Description("{@@command.link.description}")
    @CommandPermission("patreonsync.command.link")
    @Conditions("ispluginenabled")
    public void link(Player player, String email)
    {
        final UUID uuid = player.getUniqueId();

        if (this.plugin.getPatronManager().patronExists(uuid, null))
        {
            throw new InvalidCommandArgument(MessageKey.of("command.link.has_email"), false);
        }

        if (this.plugin.getPatronManager().patronExists(null, email))
        {
            throw new InvalidCommandArgument(MessageKey.of("command.link.email_used"), false);
        }

        final int code = this.plugin.getPatreonSyncManager().generateCode();
        final Patron patron = new Patron(email, code);

        this.plugin.getPatreonSyncManager().addCode(uuid, patron);
        this.plugin.getPatreonSyncManager().sendConfirmationEmail(patron);

        this.getCurrentCommandIssuer().sendMessage(MessageType.INFO, MessageKey.of("command.link.check_inbox"));
    }

    @Subcommand("dissocia")
    @Description("{@@command.unlink.description}")
    @CommandPermission("patreonsync.command.unlink")
    @Conditions("ispluginenabled")
    public void unlink(Player player)
    {
        final UUID uuid = player.getUniqueId();

        if (!this.plugin.getPatronManager().patronExists(uuid, null))
        {
            throw new InvalidCommandArgument(MessageKey.of("command.unlink.not_linked"), false);
        }

        this.plugin.getPatronManager().removePatron(uuid);
        this.getCurrentCommandIssuer().sendMessage(MessageType.INFO, MessageKey.of("command.unlink.success"));
    }

    @Subcommand("convalida")
    @Description("{@@command.validate.description}")
    @Syntax("<codice>")
    @CommandPermission("patreonsync.command.validate")
    @Conditions("ispluginenabled")
    public void validate(Player player, int code)
    {
        final UUID uuid = player.getUniqueId();

        if (!this.plugin.getPatreonSyncManager().hasCode(uuid))
        {
            throw new InvalidCommandArgument(MessageKey.of("command.validate.no_validation"), false);
        }

        if (!this.plugin.getPatreonSyncManager().isCodeValid(uuid, code))
        {
            throw new InvalidCommandArgument(MessageKey.of("command.validate.invalid_code"), false);
        }

        this.plugin.getPatronManager().addPatron(uuid, this.plugin.getPatreonSyncManager().getPatron(uuid).getEmail());
        this.plugin.getPatreonSyncManager().removeCode(uuid);

        this.getCurrentCommandIssuer().sendMessage(MessageType.INFO, MessageKey.of("command.validate.success"));
    }

    @Subcommand("reinvia")
    @Description("{@@command.resend.description}")
    @CommandPermission("patreonsync.command.resend")
    @Conditions("ispluginenabled")
    public void resend(Player player)
    {
        final UUID uuid = player.getUniqueId();

        if (!this.plugin.getPatreonSyncManager().hasCode(uuid))
        {
            throw new InvalidCommandArgument(MessageKey.of("command.resend.no_validation"), false);
        }

        final Patron patron = this.plugin.getPatreonSyncManager().getPatron(uuid);
        final int newcode = this.plugin.getPatreonSyncManager().generateCode();
        patron.setCode(newcode);

        this.plugin.getPatreonSyncManager().addCode(uuid, patron);
        this.plugin.getPatreonSyncManager().sendConfirmationEmail(patron);
        this.getCurrentCommandIssuer().sendMessage(MessageType.INFO, MessageKey.of("command.link.check_inbox"));
    }

    @Subcommand("stato")
    @Description("{@@command.status.description}")
    @CommandPermission("patreonsync.command.status")
    @Conditions("ispluginenabled")
    public void status(Player player)
    {
        final UUID uuid = player.getUniqueId();

        if (!this.plugin.getPatronManager().patronExists(uuid, null))
        {
            throw new InvalidCommandArgument(MessageKey.of("command.status.no_account"), false);
        }

        final Patron patron = this.plugin.getPatronManager().getPatron(uuid);

        this.getCurrentCommandIssuer().sendMessage(MessageType.INFO, MessageKey.of("command.status.yes_account"), "{email}", patron.getEmail());
    }

    @Subcommand("testmail")
    @Description("{@@command.testmail.description}")
    @CommandPermission("patreonsync.command.testmail")
    @Syntax("<email>")
    public void testMail(CommandSender sender, String email)
    {
        final boolean sent = this.plugin.getPatreonSyncManager().sendMail(email, "Test Mail", "Test Content.");
        final CommandIssuer issuer = this.getCurrentCommandIssuer();

        if (sent)
        {
            issuer.sendMessage(MessageType.INFO, MessageKey.of("command.testmail.sent"), "{email}", email);
        }
        else
        {
            issuer.sendError(MessageKey.of("command.testmail.unsent"));
        }
    }

    @Subcommand("reload")
    @Description("{@@command.reload.description}")
    @CommandPermission("patreonsync.command.reload")
    public void reload(CommandSender sender)
    {
        this.plugin.reloadConfig();
        this.plugin.getPatreonSyncManager().updateProperties();
        this.plugin.getPatronManager().reloadConfig();
        this.getCurrentCommandIssuer().sendMessage(MessageType.INFO, MessageKey.of("command.reload.end"));
    }

    @Subcommand("toggle")
    @Description("{@@command.toggle.description}")
    @CommandPermission("patreonsync.command.toggle")
    public void toggle(CommandSender sender)
    {
        final boolean enabled = !this.plugin.getConfig().getBoolean("enabled");

        this.plugin.getConfig().set("enabled", enabled);
        this.plugin.saveConfig();
        this.plugin.reloadConfig();

        this.getCurrentCommandIssuer().sendMessage(MessageType.INFO, MessageKey.of("command.toggle.toggled"), "{bool}", String.valueOf(enabled));
    }
}
