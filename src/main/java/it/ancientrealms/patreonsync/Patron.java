package it.ancientrealms.patreonsync;

public final class Patron
{
    private final String email;
    private int code;

    public Patron(String email, int code)
    {
        this.email = email;
        this.code = code;
    }

    public String getEmail()
    {
        return this.email;
    }

    public int getCode()
    {
        return this.code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }
}
