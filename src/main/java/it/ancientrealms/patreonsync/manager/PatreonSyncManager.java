package it.ancientrealms.patreonsync.manager;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import it.ancientrealms.patreonsync.PatreonSync;
import it.ancientrealms.patreonsync.Patron;

public final class PatreonSyncManager
{
    private final PatreonSync plugin;
    private final Properties properties;
    private Map<UUID, Patron> codes = new HashMap<>();

    public PatreonSyncManager(PatreonSync plugin)
    {
        this.plugin = plugin;
        this.properties = new Properties();
    }

    public void updateProperties()
    {
        this.properties.put("mail.smtp.auth", true);
        this.properties.put("mail.smtp.starttls.enable", this.plugin.getConfig().getBoolean("tls"));
        this.properties.put("mail.smtp.host", this.plugin.getConfig().getString("host"));
        this.properties.put("mail.smtp.port", this.plugin.getConfig().getString("port"));
        this.properties.put("mail.smtp.ssl.trust", this.plugin.getConfig().getString("host"));
    }

    public boolean sendMail(String recipient, String subject, String content)
    {
        final String username = this.plugin.getConfig().getString("username");
        final String password = this.plugin.getConfig().getString("password");

        final Session session = Session.getInstance(this.properties, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(username, password);
            }
        });

        final Message message = new MimeMessage(session);

        try
        {
            message.setFrom(new InternetAddress(this.plugin.getConfig().getString("email")));
            message.setRecipients(RecipientType.TO, InternetAddress.parse(recipient));
            message.setSubject(subject);
            message.setText(content);

            Transport.send(message);
            return true;
        }
        catch (Exception e)
        {
            this.plugin.getLogger().log(Level.SEVERE, "Impossibile inviare email a: " + recipient, e);
            return false;
        }
    }

    public boolean sendConfirmationEmail(Patron patron)
    {
        final String recipient = patron.getEmail();
        final String subject = "Verifica il tuo account Patreon";
        final String content = "Esegui questo comando in gioco per verificare il tuo account:\n\n/patreonsync:validate " + patron.getCode();

        return this.sendMail(recipient, subject, content);
    }

    public void addCode(UUID uuid, Patron patron)
    {
        this.codes.put(uuid, patron);
    }

    public boolean hasCode(UUID uuid)
    {
        return this.codes.get(uuid) == null ? false : true;
    }

    public boolean isCodeValid(UUID uuid, int code)
    {
        return this.codes.get(uuid).getCode() == code;
    }

    public Patron getPatron(UUID uuid)
    {
        return this.codes.get(uuid);
    }

    public void removeCode(UUID uuid)
    {
        this.codes.remove(uuid);
    }

    public int generateCode()
    {
        return ThreadLocalRandom.current().nextInt(100000, 1000000);
    }
}
