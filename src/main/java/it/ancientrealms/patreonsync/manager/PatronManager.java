package it.ancientrealms.patreonsync.manager;

import java.io.File;
import java.io.IOException;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import it.ancientrealms.patreonsync.PatreonSync;
import it.ancientrealms.patreonsync.Patron;

public final class PatronManager
{
    private final PatreonSync plugin;
    private File file;
    private FileConfiguration config;
    private String filename = "patrons.yml";
    private ConfigurationSection patrons;

    public PatronManager(PatreonSync plugin)
    {
        this.plugin = plugin;
        this.file = new File(plugin.getDataFolder(), this.filename);
    }

    public void initialize()
    {
        if (!this.file.exists())
        {
            this.plugin.saveResource(this.filename, false);
        }

        this.config = new YamlConfiguration();
        this.reloadConfig();
    }

    public FileConfiguration getConfig()
    {
        return this.config;
    }

    public void reloadConfig()
    {
        try
        {
            this.config.load(this.file);
            this.patrons = this.config.getConfigurationSection("patrons");
        }
        catch (IOException | InvalidConfigurationException e)
        {
            this.plugin.getLogger().log(Level.SEVERE, "Impossibile caricare il file di configurazione " + this.file, e);
        }
    }

    public void saveConfig()
    {
        try
        {
            this.config.save(this.file);
        }
        catch (IOException e)
        {
            this.plugin.getLogger().log(Level.SEVERE, "Impossibile salvare il file di configurazione " + this.file, e);
        }
    }

    public void addPatron(UUID uuid, String email)
    {
        this.config.set(String.format("patrons.%s.email", uuid), email);
        this.saveConfig();
        this.reloadConfig();
    }

    public void removePatron(UUID uuid)
    {
        this.config.set(String.format("patrons.%s", uuid), null);
        this.saveConfig();
        this.reloadConfig();
    }

    public boolean patronExists(UUID uuid, String email)
    {
        if (this.patrons == null)
        {
            return false;
        }

        final Set<String> patronskeys = this.patrons.getKeys(false);

        if (uuid != null && patronskeys.contains(uuid.toString()))
        {
            return true;
        }

        if (email != null)
        {
            for (String patronuuid : patronskeys)
            {
                final String player = this.config.getString(String.format("patrons.%s.email", patronuuid));

                if (player != null && !player.equals(email))
                {
                    return true;
                }

                if (player.equals(email))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public Patron getPatron(UUID uuid)
    {
        final String email = this.config.getString(String.format("patrons.%s.email", uuid));
        return new Patron(email, -1);
    }
}
