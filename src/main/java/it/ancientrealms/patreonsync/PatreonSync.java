package it.ancientrealms.patreonsync;

import java.util.logging.Level;

import org.bukkit.plugin.java.JavaPlugin;

import co.aikar.commands.ConditionFailedException;
import co.aikar.commands.PaperCommandManager;
import co.aikar.locales.MessageKey;
import it.ancientrealms.patreonsync.command.PatreonCommand;
import it.ancientrealms.patreonsync.manager.PatreonSyncManager;
import it.ancientrealms.patreonsync.manager.PatronManager;

public final class PatreonSync extends JavaPlugin
{
    private static PatreonSync INSTANCE;
    private PaperCommandManager commandManager;
    private PatreonSyncManager patreonSyncManager;
    private PatronManager patronManager;

    @Override
    public void onEnable()
    {
        INSTANCE = this;

        this.saveDefaultConfig();

        for (String key : this.getConfig().getKeys(false))
        {
            if (this.getConfig().getString(key).isBlank())
            {
                this.getLogger().log(Level.SEVERE, String.format("La chiave di configurazione `%s` non può essere nullo.", key));
                this.getPluginLoader().disablePlugin(this);
                break;
            }
        }

        this.patreonSyncManager = new PatreonSyncManager(this);
        this.patreonSyncManager.updateProperties();

        this.patronManager = new PatronManager(this);
        this.patronManager.initialize();

        this.commandManager = new PaperCommandManager(this);

        this.commandManager.enableUnstableAPI("brigadier");
        this.commandManager.enableUnstableAPI("help");

        this.commandManager.registerCommand(new PatreonCommand());

        this.commandManager.getCommandConditions().addCondition("ispluginenabled", (context) -> {
            if (!this.getConfig().getBoolean("enabled"))
            {
                throw new ConditionFailedException(MessageKey.of("command.toggle.in_maintenance"));
            }
        });
    }

    @Override
    public void onDisable()
    {
    }

    public static PatreonSync getInstance()
    {
        return INSTANCE;
    }

    public PaperCommandManager getCommandManager()
    {
        return this.commandManager;
    }

    public PatreonSyncManager getPatreonSyncManager()
    {
        return this.patreonSyncManager;
    }

    public PatronManager getPatronManager()
    {
        return this.patronManager;
    }
}
